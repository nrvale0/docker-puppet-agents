require 'fileutils'
require 'pathname'
require 'colorize'
require 'yaml'

task :default => 'build'

#builds = %w(puppet/ubuntu puppet/centos pe/centos pe/ubuntu)
builds = %w(pe/ubuntu)

cached_tarballs = {
 'pe/ubuntu' => {
    :uri => 'https://s3.amazonaws.com/pe-builds/released/3.3.1/puppet-enterprise-3.3.1-ubuntu-14.04-amd64.tar.gz',
    :tarball => 'puppet-enterprise-3.3.1-ubuntu-14.04-amd64.tar.gz',
  },
  'pe/centos' => {
    :uri => 'https://s3.amazonaws.com/pe-builds/released/3.3.1/puppet-enterprise-3.3.1-el-6-x86_64.tar.gz',
    :tarball => 'puppet-enterprise-3.3.1-el-6-x86_64.tar.gz',
  }
}

desc "Download and cache required PE tarballs"
task :build_cache do
  cached_tarballs.keys.each do |tarball|
    puts "\nDownloading tarball for #{tarball}..."
    unless system("cd #{File.dirname(__FILE__) + '/cache'} && wget -c #{cached_tarballs[tarball][:uri]}")
      abort "Unable to fetch #{tarball['uri']}".red
    end
    puts "#{tarball} PE tarball download complete."
  end
end

desc "Prep container build directories for 'docker build'"
task :build_prep => :build_cache do
  builds.each do |build|
    puts "\nPrepping %s build dir...\n" % (build)
    FileUtils.rm_r(build + '/buildutils', { :verbose => true })
    FileUtils.cp_r(File.dirname(__FILE__) + '/common/.', "#{build}/buildutils", { :verbose => true, })
    FileUtils.mv(build + '/buildutils/gitignore', build + "/.gitignore", { :verbose => true })

    if build.include? 'pe/' then
      puts "Copying #{build} PE tarball into #{build}/buildutils..."
      FileUtils.cp("cache/#{cached_tarballs[build][:tarball]}", 
                            "#{build}/buildutils/", { :verbose => true })
    end

    puts "%s prep complete.\n".green % (build)
  end
end

desc "Execute 'docker build"
task :build => :build_prep do
  builds.each do |container|
    flavor, os = container.split('/')
    puts "\nBuilding container %s...\n" % (container)
    pipe = IO.popen("docker -D build -t nrvale0/#{os}-#{flavor}-agent #{container}")
    while (line = pipe.gets)
      print line
    end 
    pipe.flush
    pipe.close
    unless 0 == $? then abort "Build of container %s failed!".red % (container) end
    puts "Build of #{container} complete.".green
  end
end
