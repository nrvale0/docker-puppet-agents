# docker-pe-agents

## Description
Various Docker containers with pre-installed Puppet Enterprise agent. 
PE agents consist of the Puppet agent and the MCollective server.

Agents are configured to connect to 'puppet' as a master.

## Support
https://github.com/nrvale0/docker-ubuntu-puppet-enterprise-agent/issues
